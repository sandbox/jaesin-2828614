<?php

namespace Drupal\Tests\mask\Unit;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\mask\Plugin\Field\FieldWidget\MaskTextfieldWidget;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the Mask textfield widget.
 *
 * @coversDefaultClass \Drupal\mask\Plugin\Field\FieldWidget\MaskTextfieldWidget
 * @group mask
 */
class MaskTextfieldTest extends UnitTestCase {

   public function testConstructor() {
     $field_definition = $this->getMockBuilder(BaseFieldDefinition::class)->getMock();
     ///   public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings) {

     $widget = new MaskTextfieldWidget('mask_textfield', [], $field_definition, [], []);
     self::assertInstanceOf(MaskTextfieldWidget::class, $widget);
   }
}
