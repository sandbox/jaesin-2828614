<?php

namespace Drupal\mask\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextfieldWidget;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'mask_textfield' widget.
 *
 * @FieldWidget(
 *   id = "mask_textfield",
 *   label = @Translation("Text field (masked)"),
 *   field_types = {
 *     "string"
 *   },
 * )
 */
class MaskTextfieldWidget extends TextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $main_widget = parent::formElement($items, $delta, $element, $form, $form_state);

    $element = $main_widget['value'];
    $element['#type'] = 'text_format';
    $element['#format'] = isset($items[$delta]->format) ? $items[$delta]->format : NULL;
    $element['#base_type'] = $main_widget['value']['#type'];
    return $element;
  }

}
