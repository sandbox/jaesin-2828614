/**
 * @file
 * Mask module js.
 */

/**
 * Applies masks and cloaks to textfields.
 */
(function ($, Drupal, mask) {

  /**
   * Applies masks on textfield based on either field widget settings or the
   * textfield settings. This module uses the jQuery Mask plugin
   * <https://github.com/igorescobar/jQuery-Mask-Plugin> to handle masking and
   * the js here to handle cloaking.
   *
   * Another Paragraph
   *
   * - One good point
   *   More good evidence.
   * - Another good Point
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.mask = {
    attach: function () {
      $('mask').once('mask-initialize').each(function () {
        // Initialize any masks.
        $(this).mask();
      });
    }
  };
})(jQuery, Drupal, jQuery.mask);
